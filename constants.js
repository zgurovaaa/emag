var tabs = {
  "Телефони, Таблети и Смарт технологии": [
    { tab: "Мобилни телефони", jsonName: "phones" },
    { tab: "Таблети", jsonName: "tablets" },
    { tab: "Външни батерии", jsonName: "outer-batteries" },
    { tab: "Аксесоари", jsonName: "accessories" },
    { tab: "Смарт часовници", jsonName: "smartwatches" },
    { tab: "Фитнес гривни", jsonName: "fitnessbrace" },
    { tab: "Преносими тонколони", jsonName: "portable-speaker" },
    { tab: "Smart Home и VR очила", jsonName: "smarthome-vr" }
  ],
  "Лаптопи, IT продукти и Офис": [
    { tab: "Лаптопи", jsonName: "laptops" },
    { tab: "Чанти за лаптопи", jsonName: "lapbags" },
    { tab: "Хард дискове", jsonName: "harddiscs" },
    { tab: "Охлaдителни Подложки", jsonName: "cooling-pads" },
    { tab: "Компютри", jsonName: "PCs" },
    { tab: "Аксесоари за Компютри", jsonName: "acc-for-pc" },
    { tab: "Офис столове", jsonName: "office-chairs" },
    { tab: "Всичко за офиса", jsonName: "office-stuff" }
  ],
  "ТВ, Електроника и Фото": [
    { tab: "Телевизори", jsonName: "TVs" },
    { tab: "Кабели и адаптери", jsonName: "cables-adapters" },
    { tab: "Система за домашно кино", jsonName: "home-cinema" },
    { tab: "Усилватели", jsonName: "amplifiers" },
    { tab: "Видео проектори", jsonName: "vid-proj" },
    { tab: "MP3 и MP4 Плейъри", jsonName: "mp3-mp4" },
    { tab: "Фотоапарати", jsonName: "cameras" },
    { tab: "Видеокамери", jsonName: "video-cameras" }
  ],
  "Големи електроуреди": [
    { tab: "Хладилници", jsonName: "refrigerators" },
    { tab: "Фризери", jsonName: "freezers" },
    { tab: "Перални", jsonName: "washing-mach" },
    { tab: "Сушилни", jsonName: "dryers" }
  ]
};
