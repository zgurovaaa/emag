const hasClass = (element, className) => element.classList.contains(className);

const addClass = (element, className) => element.classList.add(className);

const removeClass = (element, className) => element.classList.remove(className);

const toggleClass = (element, className) => element.classList.toggle(className);

const create = (type, attributes) => {
  var element = document.createElement(type);
  if (attributes) {
    for (var attr in attributes) {
      if (attributes.hasOwnProperty(attr)) {
        if (attr == "text") {
          element.innerHTML = attributes[attr];
        } else {
          element.setAttribute(attr, attributes[attr]);
        }
      }
    }
  }
  return element;
};

const checkPrice = (from, to) => element =>
  parseInt(element.price) >= from && parseInt(element.price) <= to;

const checkBrand = brand => element => element.brand == brand;

const addProductToDom = product => {
  const productDiv = create("div", { class: "productHolder" });
  const productImg = create("img", {
    class: "imageProduct",
    src: `../${product.img}`
  });
  const productName = create("p", {
    class: "productName",
    text: product.info
  });
  const productPrice = create("p", {
    class: "productPrice",
    text: `${product.price}.<sup>${product.supPrice}</sup> лв.`
  });

  productDiv.appendChild(productImg);
  productDiv.appendChild(productName);
  productDiv.appendChild(productPrice);
  const buyButton = create("button", {
    class: "buyButton",
    text: "КУПИ"
  });

  buyButton.addEventListener("click", e => {
    let cartProducts = JSON.parse(sessionStorage.getItem("cartProducts")) || {};
    cartProducts[product.info] = {
      product,
      count: cartProducts[product.info]
        ? cartProducts[product.info].count + 1
        : 1
    };
    sessionStorage.setItem("cartProducts", JSON.stringify(cartProducts));

    const notificationDiv = document.getElementById("notification");
    removeClass(notificationDiv, "hiddenNotification");
    addClass(notificationDiv, "showNotification");
    setTimeout(() => {
      removeClass(notificationDiv, "showNotification");
      addClass(notificationDiv, "hiddenNotification");
    }, 2000);
  });
  productDiv.appendChild(buyButton);
  return productDiv;
};

const addCartProduct = ({ product, count }, callback) => {
  const productDiv = create("div", { class: "productHolderCart" });
  const productImg = create("img", {
    class: "imageProduct",
    src: `../${product.img}`
  });
  const productName = create("span", {
    class: "productName",
    text: product.info
  });
  const priceContainer = create("div", { class: "priceContainer" });
  const productPrice = create("span", {
    class: "productPrice",
    text: `${product.price}.<sup>${product.supPrice}</sup>`
  });
  const increaseButton = create("button", {
    class: "countButtons",
    text: "+"
  });
  const deleteButton = create("button", {
    class: "countButtons",
    text: "х"
  });
  const countProduct = create("span", {
    class: "countProduct",
    text: count
  });
  const decreaseButton = create("button", {
    class: "countButtons",
    text: "-"
  });

  deleteButton.addEventListener("click", e =>
    changeCount(product.info, 0, callback)
  );
  increaseButton.addEventListener("click", e =>
    changeCount(product.info, count + 1, callback)
  );
  decreaseButton.addEventListener("click", e =>
    changeCount(product.info, count - 1, callback)
  );

  productDiv.appendChild(productImg);
  productDiv.appendChild(productName);
  productDiv.appendChild(deleteButton);
  priceContainer.appendChild(increaseButton);
  priceContainer.appendChild(countProduct);
  priceContainer.appendChild(decreaseButton);
  priceContainer.appendChild(productPrice);

  const div = create("div", { class: "productCartInfo" });
  div.appendChild(productDiv);
  div.appendChild(priceContainer);
  return div;
};
