let allNavigationTabs = document.getElementsByClassName("navLink");

const getSubTabs = tabName => tabs[tabName];

const addProductsToDom = (data, tab) => {
  const productContainer = document.getElementById("productsContainer");
  const tabProductsDiv =
    document.getElementById(tab) ||
    create("div", { class: "tabProducts", id: tab });

  for (var index = 0; index < 4; index++) {
    tabProductsDiv.appendChild(addProductToDom(data[index]));
  }

  const moreButton = create("a", {
    class: "moreButton",
    text: "ОЩЕ >",
    href: "./html/filters.html"
  });

  moreButton.addEventListener("click", e => {
    sessionStorage.setItem("subTab", tab);
  });

  productContainer.appendChild(tabProductsDiv);
  productContainer.appendChild(moreButton);
};

const replaceSubTabs = newSubTabs => {
  const container = document.getElementById("indexSideMenu");
  container.innerHTML = "";

  const unorderedList = create("ul");
  container.append(unorderedList);

  const productContainer = document.getElementById("productsContainer");
  productContainer.innerHTML = "";

  newSubTabs.forEach(subTab => {
    const listItem = create("li", {
      text: subTab.tab,
      class: "subtab"
    });
    unorderedList.appendChild(listItem);

    getProductsByCategory(subTab.jsonName).then(data => {
      const products = JSON.parse(sessionStorage.getItem("products")) || {};
      products[subTab.tab] = data;
      sessionStorage.setItem("products", JSON.stringify(products));
      addProductsToDom(data, subTab.tab);
    });

    listItem.onclick = e => {
      const previousClickedSubTab = document.getElementsByClassName(
        "subtabClicked"
      )[0];
      removeClass(previousClickedSubTab, "subtabClicked");
      addClass(listItem, "subtabClicked");
      document.getElementById(subTab.tab).scrollIntoView();
      window.scrollBy(0, -100);
    };
  });

  addClass(unorderedList.children[0], "subtabClicked");
};

const clickedTab = tab => {
  const previousTab = document.getElementsByClassName("navClickedOn")[0];
  removeClass(previousTab, "navClickedOn");
  addClass(tab, "navClickedOn");
};

Array.prototype.map.call(allNavigationTabs, tab => {
  tab.onclick = e => {
    clickedTab(tab);
    replaceSubTabs(getSubTabs(e.currentTarget.innerText));
  };
});

const currentTab = document.getElementsByClassName("navClickedOn")[0];
clickedTab(currentTab);
replaceSubTabs(getSubTabs(currentTab.innerText));
