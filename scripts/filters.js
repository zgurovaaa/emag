const subTab = sessionStorage.getItem("subTab");
const products = JSON.parse(sessionStorage.getItem("products"))[subTab];
let divBrand = document.getElementById("brand");

let brands = [];
products.forEach(element => {
  if (brands.indexOf(element.brand) == -1) {
    const check = create("input", {
      id: element.brand,
      type: "checkbox",
      name: "brand",
      value: element.brand
    });
    const label = create("label", { for: check.id, text: element.brand });
    let div = create("div");

    div.appendChild(check);
    div.appendChild(label);
    brand.appendChild(div);
    brands.push(element.brand);
  }
});

const displayProducts = products => {
  let tabProductsDiv = document.getElementById("products");
  tabProductsDiv.innerHTML = "";

  products.forEach(product => {
    tabProductsDiv.appendChild(addProductToDom(product));
  });
};

displayProducts(products);

let inputs = document.querySelectorAll("input[type=checkbox]");

const getProducts = (arrayCheckBoxes, filterBy) => {
  let result = [];

  for (let check = 0; check < arrayCheckBoxes.length; check++) {
    const currentResult = products.filter(
      filterBy == "price"
        ? checkPrice(
            parseInt(arrayCheckBoxes[check].min),
            parseInt(arrayCheckBoxes[check].max)
          )
        : checkBrand(arrayCheckBoxes[check].value)
    );
    Array.prototype.forEach.call(currentResult, element => {
      result.push(element);
    });
  }

  return result;
};

const onChangeInput = event => {
  const checkboxes = [...document.getElementsByTagName("input")];

  let pricesChecked = checkboxes.filter(
    checkbox =>
      checkbox.checked && checkbox.parentElement.parentElement.id == "prices"
  );
  let brandsChecked = checkboxes.filter(
    checkbox =>
      checkbox.checked && checkbox.parentElement.parentElement.id == "brand"
  );

  const productsDiv = document.querySelectorAll(".productHolder");
  productsDiv.forEach(div => {
    div.style.display =
      pricesChecked.length != 0 || brandsChecked.length != 0 ? "none" : "flex";
  });

  const resultPrice = getProducts(pricesChecked, "price");
  const resultBrand = getProducts(brandsChecked, "brand");

  let result = [];

  if (resultPrice.length != 0 && resultBrand.length != 0) {
    result = resultPrice.filter(product => resultBrand.indexOf(product) !== -1);
  } else {
    result = resultBrand.length != 0 ? resultBrand : resultPrice;
  }

  for (let index = 0; index < result.length; index++) {
    productsDiv.forEach(div => {
      if (div.children[1].innerHTML == result[index].info) {
        div.style.display = "flex";
      }
    });
  }
};

inputs.forEach(element => {
  element.addEventListener("change", onChangeInput);
});
